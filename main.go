package main

import (
	"archive/tar"
	"compress/gzip"
	"errors"
	"flag"
	"io"
	"log"
	"os"
	"path/filepath"
)

func main() {
	args, err := getUserArguments()
	if err != nil {
		log.Fatalln(err)
	} else {
		err := tarIt(args[0], args[1])
		if err != nil {
			log.Fatalln(err)
		}
	}
}

func tarIt(searchDir string, outputFileName string) error {
    defer timeTrack(time.Now(), "Tarball generation")
	// set up the output file
	file, err := os.Create(outputFileName)
	if err != nil {
		log.Fatalln(err)
	}
	defer file.Close()
	// set up the gzip writer
	gw := gzip.NewWriter(file)
	defer gw.Close()
	tw := tar.NewWriter(gw)
	defer tw.Close()

	err = filepath.Walk(searchDir, func(path string, f os.FileInfo, err error) error {
		fi, err := os.Stat(path)
		if err != nil {
			return err
		}
		switch mode := fi.Mode(); {
		case mode.IsDir():
			return nil
		case mode.IsRegular():
			header, err := tar.FileInfoHeader(f, f.Name())
			err = tw.WriteHeader(header)
			if err != nil {
				return err
			}
			_, err = io.Copy(tw, file)
			if err != nil {
				return err
			}
			return err
		}
		return err
	})
	return err
}

func getUserArguments() ([]string, error) {
	flag.Parse()
	args := flag.Args()
	var err error

	if len(args) == 0 {
		err = errors.New("usage: tarball /path/to/directory/we/want/to/tarball /path/to/directory/we/want/to/send/the/tarball")
	}
	if len(args) == 1 {
		args = append(args, "output.tar.gz")
	}

	return args, err
}

func timeTrack(start time.Time, name string) {
    elapsed := time.Since(start)
    log.Printf("%s took %s", name, elapsed)
}
